SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `timekoin`
--

-- --------------------------------------------------------

--
-- Table structure for table `active_peer_list`
--

CREATE TABLE IF NOT EXISTS `active_peer_list` (
  `IP_Address` varchar(46) NOT NULL,
  `domain` varchar(256) NOT NULL,
  `subfolder` varchar(256) NOT NULL,
  `port_number` smallint(5) unsigned NOT NULL,
  `last_heartbeat` int(12) unsigned NOT NULL,
  `join_peer_list` int(10) unsigned NOT NULL,
  `failed_sent_heartbeat` smallint(5) unsigned NOT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `activity_logs`
--

CREATE TABLE IF NOT EXISTS `activity_logs` (
  `timestamp` int(10) unsigned NOT NULL,
  `log` varchar(256) NOT NULL,
  `attribute` varchar(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `balance_index`
--

CREATE TABLE IF NOT EXISTS `balance_index` (
  `block` int(10) unsigned NOT NULL,
  `public_key_hash` varchar(32) NOT NULL,
  `balance` bigint(20) unsigned NOT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `generating_peer_list`
--

CREATE TABLE IF NOT EXISTS `generating_peer_list` (
  `public_key` text NOT NULL,
  `join_peer_list` int(10) unsigned NOT NULL,
  `last_generation` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `generating_peer_queue`
--

CREATE TABLE IF NOT EXISTS `generating_peer_queue` (
  `timestamp` int(10) unsigned NOT NULL,
  `public_key` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `ip_activity`
--

CREATE TABLE IF NOT EXISTS `ip_activity` (
  `timestamp` int(10) unsigned NOT NULL,
  `ip` varchar(46) NOT NULL,
  `attribute` varchar(2) NOT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `ip_banlist`
--

CREATE TABLE IF NOT EXISTS `ip_banlist` (
  `when` int(10) unsigned NOT NULL,
  `ip` varchar(46) NOT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `main_loop_status`
--

CREATE TABLE IF NOT EXISTS `main_loop_status` (
  `field_name` varchar(32) NOT NULL,
  `field_data` int(10) unsigned NOT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

--
-- Dumping data for table `main_loop_status`
--

INSERT INTO `main_loop_status` (`field_name`, `field_data`) VALUES
('foundation_heartbeat_active', 0),
('foundation_last_heartbeat', 0),
('generation_heartbeat_active', 0),
('generation_last_heartbeat', 0),
('genpeer_heartbeat_active', 0),
('genpeer_last_heartbeat', 0),
('main_heartbeat_active', 0),
('main_last_heartbeat', 0),
('peerlist_heartbeat_active', 0),
('peerlist_last_heartbeat', 0),
('queueclerk_heartbeat_active', 0),
('queueclerk_last_heartbeat', 0),
('transclerk_heartbeat_active', 0),
('transclerk_last_heartbeat', 0),
('treasurer_heartbeat_active', 0),
('treasurer_last_heartbeat', 0),
('watchdog_heartbeat_active', 0),
('watchdog_last_heartbeat', 0);

-- --------------------------------------------------------

--
-- Table structure for table `my_keys`
--

CREATE TABLE IF NOT EXISTS `my_keys` (
  `field_name` varchar(32) NOT NULL,
  `field_data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `my_keys`
--

INSERT INTO `my_keys` (`field_name`, `field_data`) VALUES
('server_private_key', 'Private Key Goes Here'),
('server_public_key', 'Public Key Goes Here');

-- --------------------------------------------------------

--
-- Table structure for table `my_transaction_queue`
--

CREATE TABLE IF NOT EXISTS `my_transaction_queue` (
  `timestamp` int(10) unsigned NOT NULL,
  `public_key` text NOT NULL,
  `crypt_data1` varchar(256) NOT NULL,
  `crypt_data2` varchar(256) NOT NULL,
  `crypt_data3` varchar(256) NOT NULL,
  `hash` varchar(64) NOT NULL,
  `attribute` varchar(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `new_peers_list`
--

CREATE TABLE IF NOT EXISTS `new_peers_list` (
  `IP_Address` varchar(46) NOT NULL,
  `domain` varchar(256) NOT NULL,
  `subfolder` varchar(256) NOT NULL,
  `port_number` smallint(5) unsigned NOT NULL,
  `poll_failures` tinyint(3) unsigned NOT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `field_name` varchar(32) NOT NULL,
  `field_data` varchar(256) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`field_name`, `field_data`) VALUES
('allow_ambient_peer_restart', '0'),
('allow_LAN_peers', '0'),
('block_check_back', '1'),
('block_check_start', '0'),
('firewall_blocked_peer', '0'),
('first_contact_server', '---ip=---domain=timekoin.com---subfolder=timekoin---port=80---end'),
('first_contact_server', '---ip=---domain=amt-wisp1.dyndns.org---subfolder=timekoin---port=88---end'),
('first_contact_server', '---ip=---domain=newdwpinc.homedns.org---subfolder=timekoin---port=80---end'),
('first_contact_server', '---ip=---domain=timekoin2.dyndns.org---subfolder=timekoin---port=80---end'),
('first_contact_server', '---ip=---domain=ind-music.com---subfolder=timekoin---port=80---end'),
('first_contact_server', '---ip=---domain=amaranthinetech.com---subfolder=timekoin---port=80---end'),
('first_contact_server', '---ip=---domain=wanip.org---subfolder=timekoin---port=80---end'),
('foundation_block_check', '0'),
('foundation_block_check_end', '0'),
('foundation_block_check_start', '0'),
('generate_currency', '0'),
('generating_peers_hash', '0'),
('generation_peer_list_no_sync', '0'),
('max_active_peers', '10'),
('max_new_peers', '15'),
('no_peer_activity', '0'),
('password', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5'),
('public_key_font_size', '3'),
('refresh_realtime_home', '5'),
('refresh_realtime_peerlist', '5'),
('refresh_realtime_queue', '5'),
('server_domain', ''),
('server_hash_code', '0'),
('server_port_number', '80'),
('server_request_max', '200'),
('server_subfolder', 'timekoin'),
('timekoin_start_time', '0'),
('time_sync_error', '0'),
('transaction_history_block_check', '0'),
('transaction_history_hash', '0'),
('transaction_queue_hash', '0'),
('username', 'ee27af0f210c0e6d81cb852197a04cb21f11bad4967365b5023ebd8cb513cbe8');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_foundation`
--

CREATE TABLE IF NOT EXISTS `transaction_foundation` (
  `block` int(10) unsigned NOT NULL,
  `hash` varchar(64) NOT NULL,
  PRIMARY KEY (`block`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_history`
--

CREATE TABLE IF NOT EXISTS `transaction_history` (
  `timestamp` int(10) unsigned NOT NULL,
  `public_key_from` text NOT NULL,
  `public_key_to` text NOT NULL,
  `crypt_data1` varchar(256) NOT NULL,
  `crypt_data2` varchar(256) NOT NULL,
  `crypt_data3` varchar(256) NOT NULL,
  `hash` varchar(64) NOT NULL,
  `attribute` varchar(2) NOT NULL,
  KEY `timestamp` (`timestamp`),
  KEY `public_key_from` (`public_key_from`(110)),
  KEY `public_key_to` (`public_key_to`(110))
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Table structure for table `transaction_queue`
--

CREATE TABLE IF NOT EXISTS `transaction_queue` (
  `timestamp` int(10) unsigned NOT NULL,
  `public_key` text NOT NULL,
  `crypt_data1` varchar(256) NOT NULL,
  `crypt_data2` varchar(256) NOT NULL,
  `crypt_data3` varchar(256) NOT NULL,
  `hash` varchar(64) NOT NULL,
  `attribute` varchar(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
