<?PHP

include 'configuration.php';
include 'function.php';

if (mysql_connect(MYSQL_IP,MYSQL_USERNAME,MYSQL_PASSWORD) &&
    mysql_select_db(MYSQL_DATABASE) &&
    mysql_table(mysql_query("SELECT GET_LOCK('timekoin_timecheck',0)")) == 1) {
  $poll_peer = filter_sql(file_get_contents("http://timekoin.net/time.php", FALSE, $context, NULL, 12));
  $my_time = time();
  
  if(abs($poll_peer - $my_time) > 15 && empty($poll_peer) == FALSE)
    {
      // Timekoin peer time is not in sync
      mysql_query("UPDATE `options` SET `field_data` = '1' WHERE `options`.`field_name` = 'time_sync_error' LIMIT 1");
    }
  else
    {
      // Timekoin peer time is in sync
      mysql_query("UPDATE `options` SET `field_data` = '0' WHERE `options`.`field_name` = 'time_sync_error' LIMIT 1");
    }
}

?>