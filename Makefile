PREFIX=/usr

all:

install:
	install 			     bin/generate-timekoin-keys $(DESTDIR)$(PREFIX)/sbin
	mkdir -p 			     $(DESTDIR)$(PREFIX)/share/timekoin/public/
	mkdir -p 			     $(DESTDIR)$(PREFIX)/share/timekoin/util/
	cp -av v1.9/*.php 		     $(DESTDIR)$(PREFIX)/share/timekoin/public/
	mv  				     $(DESTDIR)$(PREFIX)/share/timekoin/public/status.php $(DESTDIR)/var/lib/timekoin/
	cp -av util/*.php 		     $(DESTDIR)$(PREFIX)/share/timekoin/util/
	mkdir -p 			     $(DESTDIR)$(PREFIX)/share/timekoin/public/css/
	cp -av v1.9/css/*.css 		     $(DESTDIR)$(PREFIX)/share/timekoin/public/css/
	mkdir -p 			     $(DESTDIR)$(PREFIX)/share/timekoin/public/img/
	cp -av v1.9/img/*.ico 		     $(DESTDIR)$(PREFIX)/share/timekoin/public/img/
	cp -av v1.9/img/*.gif 		     $(DESTDIR)$(PREFIX)/share/timekoin/public/img/
	cp -av v1.9/img/*.jpg 		     $(DESTDIR)$(PREFIX)/share/timekoin/public/img/
	cp -av v1.9/img/*.png 		     $(DESTDIR)$(PREFIX)/share/timekoin/public/img/
	cp -av new_install_sql/timekoin.sql  $(DESTDIR)$(PREFIX)/share/dbconfig-common/data/timekoin/install/mysql
